require 'spec_helper'

describe Gitlab::Geo::JwtRequestDecoder do
  let!(:primary_node) { FactoryGirl.create(:geo_node, :primary) }
  let(:data) { { input: 123 } }
  let(:request) { Gitlab::Geo::TransferRequest.new(data) }

  subject { described_class.new(request.header['Authorization']) }

  describe '#decode' do
    it 'decodes correct data' do
      expect(subject.decode).to eq(data)
    end

    it 'fails to decode with wrong key' do
      data = request.header['Authorization']

      primary_node.secret_access_key = ''
      primary_node.save
      expect(described_class.new(data).decode).to be_nil
    end
  end
end
